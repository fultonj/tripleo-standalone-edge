#!/usr/bin/env bash

export ZONE=central
export IP=192.168.24.2
export NETMASK=24
export INTERFACE=eth0
export GATEWAY=192.168.122.1

if [[ ! $(ip a s $INTERFACE | grep $IP) ]]; then
    echo "$IP/$NETMASK must be configured on $INTERFACE before running $0"
    F=/etc/sysconfig/network-scripts/ifcfg-eth0
    echo "DEVICE=eth0" > $F
    echo "ONBOOT=yes" >> $F
    echo "TYPE=Ethernet" >> $F
    echo "IPADDR=$IP" >> $F
    echo "PREFIX=$NETMASK" >> $F
    ifup eth0
    if [[ ! $(ip a s $INTERFACE | grep $IP) ]]; then
	echo "$IP/$NETMASK still not configured on $INTERFACE"
	exit 1
    fi
fi

cat <<EOF > $HOME/standalone_parameters.yaml
resource_registry:
  OS::TripleO::Services::NovaCompute: OS::Heat::None

parameter_defaults:
  CertmongerCA: local
  CloudName: $IP
  ContainerImagePrepare:
  - set:
      ceph_image: daemon
      ceph_namespace: docker.io/ceph
      ceph_tag: v3.1.0-stable-3.1-luminous-centos-7-x86_64
      name_prefix: centos-binary-
      name_suffix: ''
      namespace: docker.io/tripleomaster
      neutron_driver: null
      tag: current-tripleo
    tag_from_label: rdo_version
  # default gateway
  ControlPlaneStaticRoutes:
    - ip_netmask: 0.0.0.0/0
      next_hop: $GATEWAY
      default: true
  Debug: true
  DeploymentUser: $USER
  DnsServers:
    - 8.8.4.4
    - 8.8.8.8
  # needed for vip & pacemaker
  KernelIpNonLocalBind: 1
  DockerInsecureRegistryAddress:
  - $IP:8787
  NeutronPublicInterface: $INTERFACE
  # domain name used by the host
  NeutronDnsDomain: localdomain
  # re-use ctlplane bridge for public net
  NeutronBridgeMappings: datacentre:br-ctlplane
  NeutronPhysicalBridge: br-ctlplane
  # enable to force metadata for public net
  #NeutronEnableForceMetadata: true
  StandaloneEnableRoutedNetworks: false
  StandaloneHomeDir: $HOME
  StandaloneLocalMtu: 1400
  # Needed if running in a VM
  GlanceBackend: swift
  StandaloneExtraConfig:
    nova::compute::libvirt::services::libvirt_virt_type: qemu
    nova::compute::libvirt::libvirt_virt_type: qemu
    nova::host: $ZONE
    cinder::host: $ZONE
EOF

if [[ ! -d ~/templates ]]; then
    ln -s /usr/share/openstack-tripleo-heat-templates ~/templates
fi

sudo openstack tripleo deploy \
  --templates ~/templates \
  --local-ip=$IP/$NETMASK \
  -e ~/templates/environments/standalone.yaml \
  -r ~/templates/roles/Standalone.yaml \
  -e $HOME/standalone_parameters.yaml \
  --output-dir $HOME \
  --standalone \
  --keep-running

# use --keep-runing so it doesn't destroy the heat processes
# which create-compute-stack-env.sh depends on
# if you need to redeploy, first kill those heat processes
