# tripleo-standalone-edge

This repository is used for local testing to deploy TripleO in
Standalone for the Edge Computing usecase as 
described in the [Split Control Plane](https://specs.openstack.org/openstack/tripleo-specs/specs/rocky/split-controlplane.html). It's based on work by 
[slagle](https://gitlab.com/slagle/tripleo-standalone-edge)
and
[EmilienM](https://gitlab.com/emacchi/tripleo-standalone-edge).

## Challenge

I would like to deploy OpenStack with TripleO but with remote
compute/ceph nodes (HCI). My remote compute/ceph nodes are on 
the Edge of the network where the workload is consumed by my
users.

Note: Ceph is not yet working.

## Architecture

```
+--------------------------+         +--------------------------+
|standalone|cpu|ceph|edge|1|         |standalone|cpu|ceph|edge|2|
+------------------------+-+         +-+------------------------+
                         |             |
                      +--+-------------+-+
                      |standalone|central|
                      +------------------+
```

In my setup I have two 8G 2vCPU Centos7 VMs on my laptop and standalone0 
is my controller while standalone1 is my "remove" compute node. Both
share a provider network.

## How to deploy

I'm using the [official documentation](https://docs.openstack.org/tripleo-docs/latest/install/containers_deployment/standalone.html) to deploy but in this repo you'll find the templates
and roles configuration.

### Step 1 - Bootstrap

Deploy the repository on each standalone host:

```bash
$ git clone https://gitlab.com/fultonj/tripleo-standalone-edge
```

Then follow the documentation to deploy tripleo-repos, and install the
latest python-tripleoclient. The whole step 1 has to be done on every
standalone node.

Alternatively, you can try running [bootstrap.sh](bootstrap.sh).

### Step 2 - Deploy Central node

Login into the standalone central node and modify the standalone-central.sh
script to match the IP address and local interface of the node.

```bash
$ cp ~/tripleo-standalone-edge/standalone-central.sh ~
$ ./standalone-central.sh
```

Once the standalone-central is deployed export its information from
its Heat stack using [create-compute-stack-env.sh](create-compute-stack-env.sh),
which will produce a file called `export_control_plane.tar.gz` which 
you should then `scp` to the remote compute node on the edge.

### Step 3 - Deploy a remote Compute/Ceph node on the Edge

Login into the standalone edge node and unpack `export_control_plane.tar.gz`
in the home directory of the user which will deploy the compute node.
Modify the standalone-edge.sh script to match the IP address and local
interface of the node. Be sure to check that the paths in `standalone-edge.sh`
exist on your system.

```bash
$ cp ~/tripleo-standalone-edge/standalone-edge.sh ~
$ ./standalone-edge.sh
```

### Step 4 - Test the deployment

Login to your central controller node and have it discover your new
compute node:

```bash
sudo docker exec -it nova_api nova-manage cell_v2 discover_hosts --verbose
```

Authenticate to the openstack install and verify it can see the
compute node:

```bash
export OS_CLOUD=standalone
openstack compute service list
openstack hypervisor list
```

It should now be possible to boot an instance on the remote compute
node.

Optionally run [test.sh](test.sh) to do the discovery and launch an instance.

## Results

These results are on my setup:

### Discover Compute Node

```
[root@standalone0 ~]# sudo docker exec -it nova_api nova-manage cell_v2 discover_hosts --verbose
Found 2 cell mappings.                                                                          
Skipping cell0 since it does not contain hosts.                                                 
Getting computes from cell 'default': 06a23f86-d76a-489f-b9ab-7d72e3a1c3e5                      
Checking host mapping for compute host 'standalone0.localdomain': 474fd6f8-223b-4d65-8a88-14b388f82888
Checking host mapping for compute host 'standalone1.localdomain': 6063dc4a-4565-4427-946b-7250e28f1a74
Creating host mapping for compute host 'standalone1.localdomain': 6063dc4a-4565-4427-946b-7250e28f1a74
Found 1 unmapped computes in cell: 06a23f86-d76a-489f-b9ab-7d72e3a1c3e5                               
[root@standalone0 ~]#
```

### List external compute node

```
[root@standalone0 ~]# openstack compute service list
+----+------------------+-------------------------+----------+---------+-------+----------------------------+
| ID | Binary           | Host                    | Zone     | Status  | State | Updated At                 |
+----+------------------+-------------------------+----------+---------+-------+----------------------------+
|  1 | nova-scheduler   | standalone0.localdomain | internal | enabled | up    | 2018-10-04T18:31:10.000000 |
|  3 | nova-consoleauth | standalone0.localdomain | internal | enabled | up    | 2018-10-04T18:31:15.000000 |
|  4 | nova-conductor   | standalone0.localdomain | internal | enabled | up    | 2018-10-04T18:31:12.000000 |
|  5 | nova-compute     | standalone0.localdomain | nova     | enabled | up    | 2018-10-04T18:31:10.000000 |
|  8 | nova-compute     | standalone1.localdomain | nova     | enabled | up    | 2018-10-04T18:31:07.000000 |
+----+------------------+-------------------------+----------+---------+-------+----------------------------+
[root@standalone0 ~]#

[root@standalone0 ~]# openstack hypervisor list
+----+-------------------------+-----------------+--------------+-------+
| ID | Hypervisor Hostname     | Hypervisor Type | Host IP      | State |
+----+-------------------------+-----------------+--------------+-------+
|  1 | standalone0.example.com | QEMU            | 192.168.24.2 | up    |
|  2 | standalone1.example.com | QEMU            | 192.168.24.3 | up    |
+----+-------------------------+-----------------+--------------+-------+
[root@standalone0 ~]#

[root@standalone0 ~]# openstack network agent list
+--------------------------------------+--------------------+-------------------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host                    | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+-------------------------+-------------------+-------+-------+---------------------------+
| 06f7c396-ea22-4d00-b0b2-b16e12857709 | L3 agent           | standalone0.localdomain | nova              | :-)   | UP    | neutron-l3-agent          |
| 11055c74-25df-4fc9-a2b6-b4b06543d3ae | DHCP agent         | standalone0.localdomain | nova              | :-)   | UP    | neutron-dhcp-agent        |
| 147cc926-224e-4d46-8f06-60c879fe9b61 | Open vSwitch agent | standalone1.localdomain | None              | :-)   | UP    | neutron-openvswitch-agent |
| 3c9cfa38-9b20-4926-aa85-8e0172b487c0 | Open vSwitch agent | standalone0.localdomain | None              | :-)   | UP    | neutron-openvswitch-agent |
| d09d3039-0a37-47bb-a7ae-0e500eef3a63 | Metadata agent     | standalone0.localdomain | None              | :-)   | UP    | neutron-metadata-agent    |
+--------------------------------------+--------------------+-------------------------+-------------------+-------+-------+---------------------------+
[root@standalone0 ~]# 
```

### Launch instance 

`openstack server create --flavor tiny --image cirros --key-name demokp --network private --security-group basic myserver2 --availability-zone nova:standalone1.localdomain`

```
[root@standalone0 ~]# openstack server list
+--------------------------------------+-----------+--------+--------------------------------------+--------+--------+
| ID                                   | Name      | Status | Networks                             | Image  | Flavor |
+--------------------------------------+-----------+--------+--------------------------------------+--------+--------+
| 1fe06f6f-cbe2-4e2c-9a97-39ca82b3ebea | myserver  | ACTIVE | private=192.168.100.10, 192.168.24.4 | cirros | tiny   |
+--------------------------------------+-----------+--------+--------------------------------------+--------+--------+
[root@standalone0 ~]# 

[root@standalone0 ~]# openstack server show 1fe06f6f-cbe2-4e2c-9a97-39ca82b3ebea | grep standalone1
| OS-EXT-SRV-ATTR:host                | standalone1.localdomain                                  |
| OS-EXT-SRV-ATTR:hypervisor_hostname | standalone1.example.com                                  |
[root@standalone0 ~]#

[root@standalone0 ~]# ssh -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -i ~/demokp.pem cirros@192.168.24.4 "lsblk"
Warning: Permanently added '192.168.24.4' (ECDSA) to the list of known hosts.
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda     253:0    0    1G  0 disk 
|-vda1  253:1    0 1015M  0 part /
`-vda15 253:15   0    8M  0 part 
[root@standalone0 ~]# 
```

As you can see, the remote compute node is registred on the central node and available to schedule servers on it.

## TODO

1. Remove the Nova compute service from the central controller node so it's not an all-in-one
2. Verify Ceph is working
3. Verify with multiple cinder-volume on edges `openstack volume service list`
4. Verify with multiple compute AZs `openstack availability zone list`
5. Make it work in [TripleO CI](https://blueprints.launchpad.net/tripleo/+spec/split-controlplane-ci)
