#!/usr/bin/env bash
# -------------------------------------------------------
# using this network option and do the steps in this doc for me
# https://docs.openstack.org/tripleo-docs/latest/install/containers_deployment/standalone.html#networking-details
# -------------------------------------------------------
SSH=1
REPO=1
INSTALL=1
CONTAINERS=1
CEPH_PREP=1
# -------------------------------------------------------
# I am using a VM on my fedora laptop and it needs to be able to ping its gateway
# so I run this to workaround the default security setup:

#sudo firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p icmp -s 192.168.122.0/24 -d 192.168.122.1 -j ACCEPT
#sudo systemctl restart firewalld.service
# -------------------------------------------------------

export FETCH=/tmp/ceph_ansible_fetch

if [[ $SSH -eq 1 ]]; then
    echo "StrictHostKeyChecking no" > ~/.ssh/config
    chmod 0600 ~/.ssh/config;
    rm -f ~/.ssh/known_hosts 2> /dev/null;
    ln -s /dev/null ~/.ssh/known_hosts
    ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
    cat /root/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
fi

if [[ $REPO -eq 1 ]]; then
    if [[ ! -d ~/rpms ]]; then mkdir ~/rpms; fi
    url=https://trunk.rdoproject.org/centos7/current/
    rpm_name=$(curl -k $url | grep python2-tripleo-repos | sed -e 's/<[^>]*>//g' | awk 'BEGIN { FS = ".rpm" } ; { print $1 }')
    rpm=$rpm_name.rpm
    curl -k -f $url/$rpm -o ~/rpms/$rpm
    if [[ -f ~/rpms/$rpm ]]; then
	sudo yum install -y ~/rpms/$rpm
	sudo -E tripleo-repos current-tripleo-dev ceph
	sudo yum repolist
	sudo yum update -y
    else
	echo "$rpm is missing. Aborting."
	exit 1
    fi
fi

if [[ $INSTALL -eq 1 ]]; then
    sudo yum install -y python-tripleoclient ceph-ansible
fi

if [[ $CONTAINERS -eq 1 ]]; then
    openstack tripleo container image prepare default \
      --output-env-file $HOME/containers-prepare-parameters.yaml
    # hack add last known working ceph tag version in my environment
    sed -i 's/ceph_tag:.*/ceph_tag:\ v3.1.0-stable-3.1-luminous-centos-7-x86_64/g' $HOME/containers-prepare-parameters.yaml
fi

if [[ $CEPH_PREP -eq 1 ]]; then
    # create a block device
    if [[ ! -e /dev/loop3 ]]; then # ensure /dev/loop3 does not exist before making it
        command -v losetup >/dev/null 2>&1 || { sudo yum -y install util-linux; }
        sudo dd if=/dev/zero of=/var/lib/ceph-osd.img bs=1 count=0 seek=7G
        sudo losetup /dev/loop3 /var/lib/ceph-osd.img
    elif [[ -f /var/lib/ceph-osd.img ]]; then #loop3 and ceph-osd.img exist
        echo "warning: looks like ceph loop device already created. Trying to continue"
    else
        echo "error: /dev/loop3 exists but not /var/lib/ceph-osd.img. Exiting."
        exit 1
    fi
    sgdisk -Z /dev/loop3
    sudo lsblk
    if [[ ! -d $FETCH ]]; then
	mkdir $FETCH
    fi
    chmod 777 $FETCH

    cat <<EOF > $HOME/ceph_parameters.yaml
parameter_defaults:
  CephAnsibleDisksConfig:
    devices:
      - /dev/loop3
    journal_size: 1024
  CephAnsibleExtraConfig:
    osd_scenario: collocated
    osd_objectstore: filestore
    cluster_network: 192.168.24.0/24
    public_network: 192.168.24.0/24
  CephAnsiblePlaybookVerbosity: 3
  CephPoolDefaultSize: 1
  CephPoolDefaultPgNum: 32
  LocalCephAnsibleFetchDirectoryBackup: $FETCH
EOF
fi
